<?php


namespace App\Tests\Entity;


use App\Entity\Phone;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class PhoneTest extends TestCase
{

    /**
     * @param string $name
     * @param int $price
     * @param string $color
     * @param string|null $description
     * @dataProvider phoneDataProvider
     */
    public function testPhoneReceivesValues(string $name, int $price, string $color, ?string $description)
    {
        $phone = new Phone($name, $price, $color, $description);
        $this->assertSame($name, $phone->getName());
        $this->assertSame($price, $phone->getPrice());
        $this->assertSame($color, $phone->getColor());

    }

    /**
     * @return \Generator
     */
    public function phoneDataProvider()
    {
        yield ["iPhone 5", 300, "White", null];
        yield ["iPhone 6", 800, "Gold", null];
        yield ["Samsung S8", 700, "Black", null];
    }

    /**
     * @param string $name
     * @param int $price
     * @param string $color
     *
     * @param string|null $description
     * @expectedException \Exception
     * @dataProvider phoneDataProvider
     */
    /*public function testTooLongPhoneName(string $name, int $price, string $color, ?string $description )
    {*/
        /*$validator = $this
            ->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock()
        ;

        $violations = $validator->validate(
            'B',
            [
                new Length(["min" => 2]),
                new NotBlank()
            ]
        );

        $this->assertCount(1, $violations);
        $this->assertEquals(
            'This value is too short. It should have 10 characters or more.',
            $violations[0]->getMessage()
        );*/


        /**/

    /**
     * @param string $name
     * @param int $price
     * @param string $color
     *
     * @param string|null $description
     * @expectedException \Exception
     * @dataProvider phoneDataProvider
     */
    /*public function testTooShortPhoneName(string $name, int $price, string $color, ?string $description)
    {

        $phone = new Phone($name, $price, $color, $description);
        $phone->setName("a");

    }*/
}
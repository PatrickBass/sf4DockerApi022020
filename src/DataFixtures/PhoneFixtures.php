<?php

namespace App\DataFixtures;

use App\Document\Phone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PhoneFixtures extends Fixture
{
    private $names = ['iPhone', 'Samsung'];
    private $colors = ['Black', 'White', 'Red', 'Gold'];

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i=0; $i < 200; $i++) {
            $phone = new Phone("bidule", 100, "Black", "blablabla");
            $phone->setName($this->names[rand(0,1)].' '.rand(5, 11));
            $phone->setDescription(sprintf("Ce téléphones dispose de %d fonctionnalités récentes", rand(10, 20)));
            $phone->setPrice(rand(500, 1200));
            $phone->setColor($this->colors[rand(0, 3)]);
            $manager->persist($phone);
        }

        $manager->flush();
    }

}

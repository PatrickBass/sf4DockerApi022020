<?php

namespace App\Document;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="App\Repository\PhoneRepository", db="api022020", collection="phones")
 */
class Phone
{
    /**
     * @MongoDB\Id
     * @Groups({"list", "show"})
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     * @Groups({"list", "show"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="100")
     * @var string
     */
    private $name;

    /**
     * @MongoDB\Field(type="float")
     * @Groups({"list", "show"})
     * @Assert\Range(min="100", max="1500")
     * @Assert\NotBlank()
     * @var float
     */
    private $price;

    /**
     * @MongoDB\Field(type="string")
     * @Groups({"show"})
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="50")
     * @var string
     */
    private $color;

    /**
     * @MongoDB\Field(type="string")
     * @Groups({"show"})
     * @var string|null
     */
    private $description;

    /**
     * Phone constructor.
     * @param string $name
     * @param float $price
     * @param string $color
     * @param string $description
     */
    public function __construct(string $name, float $price, string $color, string $description)
    {
        $this->name = $name;
        $this->price = $price;
        $this->color = $color;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}

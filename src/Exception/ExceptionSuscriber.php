<?php


namespace App\Exception;



use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionSuscriber implements EventSubscriberInterface
{
    /** @var NormalizerInterface[]  */
    private $normalizers;

    /** @var SerializerInterface */
    private $serializer;

    /**
     * ExceptionSuscriber constructor.
     * @param NormalizerInterface[] $normalizers
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer, NormalizerInterface ...$normalizers)
    {
        $this->normalizers = $normalizers;
        $this->serializer = $serializer;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 255]
            ]
        ];
    }

    public function processException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        dd($exception);

        $result = null;

        foreach ($this->normalizers as $normalizer) {
            if ($normalizer->supports($exception)) {
                $result = $normalizer->normalize($exception);
                //dd($normalizer);
                break;
            }
        }

        if (!$result) {
            $result = [
                'code' => $exception->getStatusCode() ?? Response::HTTP_BAD_REQUEST,
                'body' => [
                    'code' => $exception->getStatusCode() ?? Response::HTTP_BAD_REQUEST,
                    'message' => $exception->getMessage()
                ]
            ];
        }

        $body = $this->serializer->serialize($result['body'], 'json');

        $event->setResponse(new Response($body, $result['code']));

    }

    public function addNormalizer(NormalizerInterface $normalizer)
    {
        $this->normalizers[] = $normalizer;
    }
}
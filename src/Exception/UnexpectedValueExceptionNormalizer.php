<?php


namespace App\Exception;


class UnexpectedValueExceptionNormalizer extends AbstractNormalizer
{
    public function normalize(\Exception $exception)
    {
        $result = parent::normalize($exception);
        $result['code'] = $exception->getCode();
        $result['message'] = $exception->getMessage();

        return $result;
    }
}
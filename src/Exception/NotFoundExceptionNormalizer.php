<?php


namespace App\Exception;


use Symfony\Component\HttpFoundation\Response;

class NotFoundExceptionNormalizer extends AbstractNormalizer
{
    public function normalize(\Exception $exception)
    {
        $result = parent::normalize($exception);

        $result['code'] = $exception->getStatusCode() ?? Response::HTTP_NOT_FOUND;
        $result['body'] = [
            'code' => $exception->getStatusCode() ?? Response::HTTP_NOT_FOUND,
            'message' => $exception->getMessage()
        ];

        return $result;
    }
}
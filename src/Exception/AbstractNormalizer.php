<?php


namespace App\Exception;


abstract class AbstractNormalizer implements NormalizerInterface
{
    /** @var \Exception[] */
    public $typesException;

    /**
     * AbstractNormalizer constructor.
     * @param \Exception[] $typesException
     */
    public function __construct(array $typesException)
    {
        $this->typesException = $typesException;
    }


    /**
     * {@inheritDoc}
     */
    public function normalize(\Exception $exception)
    {
        return [];
    }


    /**
     * {@inheritDoc}
     */
    public function supports(\Exception $exception)
    {
        //dd(get_class($exception));
        return in_array(get_class($exception), $this->typesException);
    }
}
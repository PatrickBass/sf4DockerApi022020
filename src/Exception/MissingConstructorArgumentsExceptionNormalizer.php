<?php


namespace App\Exception;


use Symfony\Component\HttpFoundation\Response;

class MissingConstructorArgumentsExceptionNormalizer extends AbstractNormalizer
{
    public function normalize(\Exception $exception)
    {
        $result = parent::normalize($exception);

        $result['code'] = Response::HTTP_BAD_REQUEST;
        $result['body'] = [
            'code' => Response::HTTP_BAD_REQUEST,
            'message' => $exception->getMessage()
        ];

        return $result;
    }
}
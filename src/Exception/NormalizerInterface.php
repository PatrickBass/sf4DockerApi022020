<?php


namespace App\Exception;


/**
 * Interface NormalizerInterface
 * @package App\Exception
 */
interface NormalizerInterface
{
    /**
     * @param \Exception $exception
     * @return array
     */
    public function normalize(\Exception $exception);

    /**
     * @param \Exception $exception
     * @return bool
     */
    public function supports(\Exception $exception);
}
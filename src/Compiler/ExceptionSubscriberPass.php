<?php


namespace App\Compiler;


use App\Exception\ExceptionSuscriber;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ExceptionSubscriberPass implements CompilerPassInterface
{

    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        $exceptionSubscriberDefinition = $container->getDefinition(ExceptionSuscriber::class); //ExceptionSubscriber::class
        $normalizers = $container->findTaggedServiceIds('@App\Exception\NormalizerInterface');

        foreach ($normalizers as $id => $tags) {
            $exceptionSubscriberDefinition->addMethodCall('addNormalizer', [new Reference($id)]);
        }
    }
}
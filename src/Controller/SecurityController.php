<?php

namespace App\Controller;

use App\Document\User;
use Doctrine\Migrations\Configuration\Exception\JsonNotValid;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/security", name="security")
     */
    public function index()
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {

        $userData = json_decode($request->getContent(), 'true');

        foreach ($userData as $key => $value) {

            if ($key && $value) {
                $user = new User();

                $user->setPassword($passwordEncoder->encodePassword($user, $userData['password']));
                $user->setRoles($user->getRoles());

                /*$setter = 'set'.ucfirst($key);
                $user->$setter($value);
                if ($key === 'roles') {
                    dd($value);
                    $user->$setter($passwordEncoder->encodePassword($user, $userData['password']));
                } elseif ($key === 'roles') {
                    $user->$setter($user->getRoles());
                }*/

                //dd($user);

                //$violations = $validator->validate($user);

                //dd($violations);

                /*if ($violations) {
                    $violations = $serializer->serialize($violations, 'json');
                    return new Response($violations, Response::HTTP_INTERNAL_SERVER_ERROR, [
                        'Content-Type' => 'application/json'
                    ]);
                }*/



            } else {
                throw new \RuntimeException("Please provide your credentials");
            }

            $entityManager->persist($user);
            $entityManager->flush();

            dd($user);

            $data = [
                'code' => Response::HTTP_CREATED,
                'message' => 'A new user has been created!'
            ];

            $data = $serializer->serialize($data, 'json');

            return new Response($data, Response::HTTP_CREATED, [
                'Content-Type' => 'application/json'
            ]);
        }
    }

    public function login()
    {
        $user = $this->getUser();

        return $this
            ->json([
                'email' => $user->getEmail(),
                'password' => $user->getPassword()
            ])
        ;
    }
}

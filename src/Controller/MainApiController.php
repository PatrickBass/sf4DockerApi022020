<?php

namespace App\Controller;

use App\Document\Phone;
use App\Repository\PhoneRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\LockException;
use Doctrine\ODM\MongoDB\Mapping\MappingException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Dumper\GraphvizDumper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MainApiController extends AbstractController
{
    /** @var LoggerInterface */
    private $logger;

    private $names = ['iPhone', 'Samsung'];
    private $colors = ['Black', 'White', 'Red', 'Gold'];

    /**
     * MainApiController constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/mongoTest", methods={"GET"})
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function mongoTest(DocumentManager $dm, PhoneRepository $repository, SerializerInterface $serializer)
    {
        $phonesList = [];
        for ($i=0; $i < 200; $i++) {
            $phone = new Phone("bidule", 100, "Black", "blablabla");
            $phone->setName($this->names[rand(0,1)].' '.rand(5, 11));
            $phone->setDescription(sprintf("Ce téléphones dispose de %d fonctionnalités récentes", rand(10, 20)));
            $phone->setPrice(rand(500, 1200));
            $phone->setColor($this->colors[rand(0, 3)]);
            $dm->persist($phone);
            $phonesList[] = $phone;
        }

        $dm->flush();

        $data = $serializer->serialize($repository->findAll(), 'json');
        return new Response($data, Response::HTTP_OK);
    }

    /**
     * @Route("/dumpgraphdot", methods={"GET"})
     *
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function dumpgraphdot()
    {
        $dumper = new GraphvizDumper();
        $definition = $dumper->dump($definition);

        //$data = $serializer->serialize($repository->findAll(), 'json');
        return new Response($definition);
    }

    /**
     * @Route("/phones/{id<\d+>}", name="show_phone", methods={"GET"})
     * @param Phone $phone
     * @param PhoneRepository $repository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function showPhone(Phone $phone, PhoneRepository $repository, SerializerInterface $serializer)
    {
        try {
            $phone = $repository->find($phone->getId());
        } catch (LockException $e) {
            $this->logger->error($e->getMessage());
            return new Response('No phone found for id '.$phone->getId());
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage());
            return new Response('No phone found for id '.$phone->getId());
        }

        $data = $serializer->serialize($phone, 'json', [
            'groups' => ['show']
        ]);



        return new Response($data, Response::HTTP_OK, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * @param Phone $phone
     * @param Request $request
     * @param DocumentManager $dm
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param PhoneRepository $repository
     * @return Response
     *
     * @Route("/Phones/{id<\d+>}", name="update_phone", methods={"PUT"})
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws MappingException
     */
    public function updatePhone(
        Phone $phone,
        Request $request,
        DocumentManager $dm,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        PhoneRepository $repository
    ) {

        $phoneToUpdate = $repository->find($phone->getId());

        $data = $serializer->deserialize($request->getContent(), 'array', 'json');

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ($key && $value) {
                    $key = ucfirst($key);
                    $setter = 'set'.$key;
                    $phoneToUpdate->$setter($value);
                }
            }
        }


        $violations = $validator->validate();

        if ($violations) {
            $data = $serializer->serialize($violations, 'json');
            return new Response($data, Response::HTTP_INTERNAL_SERVER_ERROR, [
                'Content-Type' => 'application/json'
            ]);
        }

        $dm->flush();

        $data = [
            "code" => Response::HTTP_OK,
            "message" => "Phone has been updated successfully!"
        ];

        return new Response($data, Response::HTTP_OK, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * @param Phone $phone
     * @param Request $request
     * @param DocumentManager $dm
     * @param SerializerInterface $serializer
     * @param PhoneRepository $repository
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @Route("/Phones/{id<\d+>}", name="update_phone", methods={"PUT"})
     */
    public function deletePhone(
        Phone $phone,
        Request $request,
        DocumentManager $dm,
        SerializerInterface $serializer,
        PhoneRepository $repository
    ) {

        try {
            $phoneToDelete = $repository->find($phone->getId());
        } catch (LockException $e) {
            $this->logger->error($e->getMessage());
            return new Response('No phone found with id '.$phone->getId());
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage());
            return new Response('No phone found with id '.$phone->getId());
        }

        $data = $serializer->deserialize($request->getContent(), 'array', 'json');

        $dm->remove($phoneToDelete);

        $dm->flush();

        $data = [
            "code" => Response::HTTP_NO_CONTENT,
            "message" => "Phone has been deleted successfully!"
        ];

        return new Response($data, Response::HTTP_NO_CONTENT, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * @Route("/phones/{page<\d+>?1}", name="main_api", methods={"GET"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param PhoneRepository $repository
     * @return Response
     */
    public function index(Request $request, SerializerInterface $serializer, PhoneRepository $repository)
    {
        $routeParams = $request->attributes->get('_route_params');

        $page = $routeParams['page'];

        if (is_null($page) || $page < 1) {
            $page = 1;
        }

        $limit = 5;
        //getenv('LIMIT')
        $phones = $repository->findPhonesPerPageLimit($page, $limit);
        $phonesData = null;
        if ($phones) {
            $phonesData = $serializer->serialize($phones, 'json', [
                'groups' => ['list']
            ]);
        }

        if (null === $phonesData) {
            $phonesData = [
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => "Une erreur est survenue lors de la récupération des données du téléphone en bdd"
            ];
        }

        $response = new Response($phonesData, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        $response->setCache([
            'etag' => md5(uniqid()),
            'max_age' => 3600,
            's_maxage' => 3600,
            'public' => true
        ]);

        return $response;
    }

    /**
     * @Route("/phones", name="new_phone", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function newPhone(Request $request, SerializerInterface $serializer, DocumentManager $dm, ValidatorInterface $validator)
    {
        $phone = $serializer->deserialize($request->getContent(), Phone::class, 'json');

        $errors = $validator->validate($phone);

        //dd($errors);

        if (count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, Response::HTTP_INTERNAL_SERVER_ERROR, [
                'Content-Type' => 'application/json'
            ]);
        }

        $dm->persist($phone);
        $dm->flush();

        $data = [
            'code' => Response::HTTP_CREATED,
            'message' => "New phone object has been added"
        ];

        return new JsonResponse($data, Response::HTTP_CREATED);
    }


    /*public function newPhone(Request $request, SerializerInterface $serializer, EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $phone = $serializer->deserialize($request->getContent(), Phone::class, 'json');

        $errors = $validator->validate($phone);

        //dd($errors);

        if (count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, Response::HTTP_INTERNAL_SERVER_ERROR, [
                'Content-Type' => 'application/json'
            ]);
        }

        $entityManager->persist($phone);
        $entityManager->flush();

        $data = [
            'code' => Response::HTTP_CREATED,
            'message' => "New phone object has been added"
        ];

        return new JsonResponse($data, Response::HTTP_CREATED);
    }*/
}
